#include "Helper.h"


void Helper::trim(string &str)
{
	rtrim(str);
	ltrim(str);

}

//cut the spaces from str from 
void Helper::rtrim(string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos) //if his not get to the end
	{
		str = str.substr(0, endpos + 1); //copy to new string
	}
}

void Helper::ltrim(string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

//get word from str
vector<string> Helper::get_words(string &str)
{
	vector<string> words;
	std::istringstream strStream(str);
	copy(istream_iterator<string>(strStream),
		istream_iterator<string>(),
		back_inserter(words));

	return words;
}

