#pragma once

#include<map>
#include<set>
#include<iostream>
#include<string>
#include<algorithm>
#include <utility> 
#include <windows.h>
#include <string>
#include <iostream>


#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")
#include <cstring>

using namespace std;

//for the LS commend
typedef UINT(CALLBACK* LPFNDLLFUNC1)(DWORD, UINT);
typedef int (CALLBACK* theFunctionType)(void);




class Windows_shell
{
public:
	Windows_shell();
	~Windows_shell();

	void pwdCommand();
	void cdCommand(string path);
	int createCommand(string name);
	void DisplayError(LPTSTR lpszFunction);
	int lsCommand(string name);
	void getProcAddressSecret(string name);
	void loadLibrarySecret();
	int creatProcessExe(string name);

	int process_shell();
};

