#include "Windows_shell.h"
#include "Helper.h"

#define BUFSIZE MAX_PATH

//for the exe coomand
HANDLE g_hChildStd_IN_Rd = NULL;
HANDLE g_hChildStd_IN_Wr = NULL;
HANDLE g_hChildStd_OUT_Rd = NULL;
HANDLE g_hChildStd_OUT_Wr = NULL;

HANDLE g_hInputFile = NULL;


/*
This function is the constructor, she will initialize all of the fields
INPUT: nun
OUTPUT: void
*/
Windows_shell::Windows_shell()
{
}

/*
This function is the destructor, she will delete all the memory that dynamically assigned
INPUT: nun
OUTPUT: nothing, delete evereting
*/
Windows_shell::~Windows_shell()
{
}

/*
This function will displays the path of the current folder to the user.
INPUT: nun
OUTPUT: void
https://msdn.microsoft.com/en-us/library/windows/desktop/aa364934(v=vs.85).aspx
*/
void Windows_shell::pwdCommand()
{
	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;

	dwRet = GetCurrentDirectory(BUFSIZE, Buffer);

	cout << Buffer << endl;

}


/*
This function will changes the current folder to the resulting path.
INPUT: the path to change
OUTPUT: void
https://msdn.microsoft.com/en-us/library/system.io.directory.setcurrentdirectory(v=vs.110).aspx
*/
void Windows_shell::cdCommand(string path)
{

	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;


	LPCSTR newPath = path.c_str();

	dwRet = GetCurrentDirectory(BUFSIZE, Buffer);

	if (dwRet == 0)
	{
		printf("GetCurrentDirectory failed (%d)\n", GetLastError());
	}
	if (dwRet > BUFSIZE)
	{
		printf("Buffer too small; need %d characters\n", dwRet);
	}

	if (!SetCurrentDirectory(newPath))
	{
		printf("SetCurrentDirectory failed (%d)\n", GetLastError());
	}

	if (!SetCurrentDirectory(Buffer))
	{
		printf("SetCurrentDirectory failed (%d)\n", GetLastError());
	}

	DWORD newDirect = SetCurrentDirectory(newPath); //set to the path

	

}

/*
This function will Accepts as a parameter a filename and creates it. If the file exists, creates a new file instead.
INPUT: the name of the file
OUTPUT: int - if there is error
https://msdn.microsoft.com/en-us/library/windows/desktop/aa363858(v=vs.85).aspx
*/
int Windows_shell::createCommand(string name)
{
	HANDLE hFile;
	char DataBuffer[] = "This is some test data to write to the file.";
	DWORD dwBytesToWrite = (DWORD)strlen(DataBuffer);
	DWORD dwBytesWritten = 0;
	BOOL bErrorFlag = FALSE;

	LPCSTR newName = name.c_str();

	int returnVal = 0;

	hFile = CreateFile(newName,                // name of the write
		GENERIC_WRITE,          // open for writing
		0,                      // do not share
		NULL,                   // default security
		CREATE_ALWAYS,             // create always file, if he exist he create a new one
		FILE_ATTRIBUTE_NORMAL,  // normal file
		NULL);                  // no attr. template


	if (hFile == INVALID_HANDLE_VALUE)
	{
		//DisplayError(TEXT("CreateFile"));
		printf(TEXT("Terminal failure: Unable to open file \"%s\" for write.\n"),newName);
		returnVal = 1;
	}

	printf(TEXT("Writing %d bytes to %s.\n"), dwBytesToWrite, newName);

	bErrorFlag = WriteFile(
		hFile,           // open file handle
		DataBuffer,      // start of data to write
		dwBytesToWrite,  // number of bytes to write
		&dwBytesWritten, // number of bytes that were written
		NULL);            // no overlapped structure

	if (FALSE == bErrorFlag)
	{
		//DisplayError(TEXT("WriteFile"));
		printf("Terminal failure: Unable to write to file.\n");
	}
	else
	{
		if (dwBytesWritten != dwBytesToWrite)
		{
			// This is an error because a synchronous write that results in
			// success (WriteFile returns TRUE) should write all data as
			// requested. This would not necessarily be the case for
			// asynchronous writes.
			printf("Error: dwBytesWritten != dwBytesToWrite\n");
		}
		else
		{
			printf(TEXT("Wrote %d bytes to %s successfully.\n"), dwBytesWritten, newName);
		}
	}

	CloseHandle(hFile); //close the hendel

	return returnVal;
}

/*
This function will displays an error message in a dialog.
INPUT: the error
OUTPUT: void
https://msdn.microsoft.com/en-us/library/office/bb290808(v=office.12).aspx
*/
void Windows_shell::DisplayError(LPTSTR lpszFunction)
{
	// Routine Description:
	// Retrieve and output the system error message for the last-error code
	
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0,
		NULL);

	lpDisplayBuf =
		(LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf)
			+ lstrlen((LPCTSTR)lpszFunction)
			+ 40) // account for format string
			* sizeof(TCHAR));

	/*if (FAILED(StringCchPrintf((LPTSTR)lpDisplayBuf,
	LocalSize(lpDisplayBuf) / sizeof(TCHAR),
	TEXT("%s failed with error code %d as follows:\n%s"),
	lpszFunction,
	dw,
	lpMsgBuf)))
	{
	printf("FATAL ERROR: Unable to output error code.\n");
	}*/

	printf(TEXT("ERROR: %s\n"), (LPCTSTR)lpDisplayBuf);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);

}


/*
This function will Write the names of all the files in the given folder.
INPUT: the PATH of the folder
OUTPUT: int - if there is error
https://msdn.microsoft.com/en-us/library/windows/desktop/aa365200(v=vs.85).aspx
*/
int Windows_shell::lsCommand(string name)
{
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	LPCSTR newName = name.c_str();

	int returnVal = 0;


	// If the directory is not specified as a command-line argument,
	// print usage.


	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.

	StringCchLength(newName, MAX_PATH, &length_of_arg);

	if (length_of_arg > (MAX_PATH - 3))
	{
		_tprintf(TEXT("\nDirectory path is too long.\n"));
		returnVal = 1;
	}

	_tprintf(TEXT("\nTarget directory is %s\n\n"), newName);

	// Prepare string for use with FindFile functions.  First, copy the
	// string to a buffer, then append '\*' to the directory name.

	StringCchCopy(szDir, MAX_PATH, newName);
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.

	hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		//DisplayErrorBox(TEXT("FindFirstFile"));
		printf("FindFirstFile");
		//return dwError;
		returnVal = 1;
	}

	// List all the files in the directory with some info about them.

	int i = 0;

	do
	{
		if (/*ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY &&*/ i > 1) //To skip the dots
		{
			_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
		}
		else if (!ffd.dwFileAttributes & !FILE_ATTRIBUTE_DIRECTORY)
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			_tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
		}
		i++;
	} while (FindNextFile(hFind, &ffd) != 0);

	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		//DisplayErrorBox(TEXT("FindFirstFile"));
		printf("FindFirstFile");
	}

	FindClose(hFind);
	//return dwError;

	return returnVal;
}

/*
This function will Loads the attached dll. Runs the function: TheAnswerToLifeTheUniverseAndEverything. And prints out its result.
INPUT: the name of the dll
OUTPUT: void
https://msdn.microsoft.com/en-us/library/ms810279.aspx
*/
void Windows_shell::getProcAddressSecret(string name)
{

	HINSTANCE hDLL;               // Handle to DLL  
	LPFNDLLFUNC1 lpfnDllFunc1;    // Function pointer  
	DWORD dwParam1 = 0;
	UINT  uParam2 = 0, uReturnVal;

	hDLL = LoadLibrary("MyDLL");
	if (hDLL != NULL)
	{
		lpfnDllFunc1 = (LPFNDLLFUNC1)GetProcAddress(hDLL,
			"DLLFunc1");
		if (!lpfnDllFunc1)
		{
			// handle the error  
			FreeLibrary(hDLL);
			//return SOME_ERROR_CODE;
		}
		else
		{
			// call the function  
			uReturnVal = lpfnDllFunc1(dwParam1, uParam2);
		}
	}

}

/*
This function will Loads the attached dll.
INPUT: nun
OUTPUT: void
https://msdn.microsoft.com/en-us/library/ms810279.aspx
*/
void Windows_shell::loadLibrarySecret()
{
	BOOL freeResult, runTimeLinkSuccess = FALSE;
	HINSTANCE dllHandle = NULL;
	theFunctionType theFunc = NULL;

	//Load the dll and keep the handle to it
	dllHandle = LoadLibrary("Secret.dll");

	// If the handle is valid, try to get the function address. 
	if (NULL != dllHandle)
	{
		//Get pointer to our function using GetProcAddress:
		theFunc = (theFunctionType)GetProcAddress(dllHandle,
			"TheAnswerToLifeTheUniverseAndEverything"); //call the function

		// If the function address is valid, call the function. 
		if (runTimeLinkSuccess = (NULL != theFunc))
		{
			//LPCTSTR myArtist = "Duchamp";
			int retVal = theFunc();
			cout << "The answer of the best question in the world: "  << retVal << endl;
		}

		//Free the library:
		freeResult = FreeLibrary(dllHandle);
	}

	//If unable to call the DLL function, use an alternative. 
	if (!runTimeLinkSuccess)
		printf("message via alternative method\n");
}

/*
This function will create process for exe file
INPUT: the name of the file
OUTPUT: int - if there error
https://msdn.microsoft.com/en-us/library/ms810279.aspx
*/
int Windows_shell::creatProcessExe(string name) 
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	//string newName = name;
	LPSTR newName =  const_cast<char *>(name.c_str());
	int returnVal = 0;

	
	// Start the child process. 
	if (!CreateProcess(NULL,   // No module name (use command line)
		newName,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		returnVal = 1;
		//return;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return returnVal;
}


void WriteToPipe(void)

// Read from a file and write its contents to the pipe for the child's STDIN.
// Stop when there is no more data. 
{
	DWORD dwRead, dwWritten;
	CHAR chBuf[BUFSIZE];
	BOOL bSuccess = FALSE;

	for (;;)
	{
		bSuccess = ReadFile(g_hInputFile, chBuf, BUFSIZE, &dwRead, NULL);
		if (!bSuccess || dwRead == 0) break;

		bSuccess = WriteFile(g_hChildStd_IN_Wr, chBuf, dwRead, &dwWritten, NULL);
		if (!bSuccess) break;
	}

	// Close the pipe handle so the child process stops reading. 

	if (!CloseHandle(g_hChildStd_IN_Wr))
		printf("StdInWr CloseHandle");
		//ErrorExit(TEXT("StdInWr CloseHandle"));
		
}

void ReadFromPipe(void)

// Read output from the child process's pipe for STDOUT
// and write to the parent process's pipe for STDOUT. 
// Stop when there is no more data. 
{
	DWORD dwRead, dwWritten;
	CHAR chBuf[BUFSIZE];
	BOOL bSuccess = FALSE;
	HANDLE hParentStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	for (;;)
	{
		bSuccess = ReadFile(g_hChildStd_OUT_Rd, chBuf, BUFSIZE, &dwRead, NULL);
		if (!bSuccess || dwRead == 0) break;

		bSuccess = WriteFile(hParentStdOut, chBuf,
			dwRead, &dwWritten, NULL);
		if (!bSuccess) break;
	}
}

void ErrorExit(PTSTR lpszFunction)

// Format a readable error message, display a message box, 
// and exit from the application.
{
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
	ExitProcess(1);



	//return 0;
}


void DisplayErrorBox(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message and clean up

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}





int Windows_shell::process_shell()
{
	string fullCommand;
	
	while (1)
	{
		TCHAR Buffer[BUFSIZE];
		DWORD dwRet = GetCurrentDirectory(BUFSIZE, Buffer);


		cout <<  Buffer << " :) ";

		getline(cin, fullCommand);

		Helper::trim(fullCommand);
		vector<string> wordsVec = Helper::get_words(fullCommand); //To every commend


		if (wordsVec[0] == "pwd")
		{
			pwdCommand();
		}
		if (wordsVec[0] == "cd")
		{
			cdCommand(wordsVec[1]);
			
		}
		if (wordsVec[0] == "create")
		{
			int returnCreate = createCommand(wordsVec[1]);
			if (returnCreate == 1)
			{
				cout << "Erorr in creating file" << endl;
			}

		}
		if (wordsVec[0] == "ls")
		{
			int returnLs = lsCommand(wordsVec[1]);
			if (returnLs == 1)
			{
				cout << "Erorr with ls command" << endl;
			}

		}
		if (wordsVec[0] == "secret")
		{
			loadLibrarySecret();

		}
		if (wordsVec[0] == "exe")
		{
			int returnExe = creatProcessExe(wordsVec[1]);
			if (returnExe == 1)
			{
				cout << "Erorr with exe command" << endl;
			}

		}
		else
		{
			cout << "Command not found, please try again..." << endl;
		}

	}



	return 0;
}
